#!/usr/bin/env bash

sudo apt-get install -y python3-pip python3-dev
sudo apt-get install libopenblas-dev liblapack-dev liblapacke-dev
sudo apt-get install libhdf5-dev
sudo apt-get install python3-h5py
sudo apt-get install libfreetype6-dev
pip3 install scikit-image --user
pip3 install prefetch_generator --user
pip3 install keras --user
pip3 install Pillow --user


file /usr/local/cuda | grep cuda-8
if [ $? -eq 0 ]; then
	cudaVersion=8
else
	cudaVersion=9
fi

if [ $cudaVersion -eq 8 ]; then
	echo 'Cuda 8'
	sudo pip3 install tf_wheels/tensorflow-1.3.0-cp35-cp35m-linux_aarch64.whl
elif [ $cudaVersion -eq 9 ]; then
	echo 'Cuda 9'
	sudo pip3 install tf_wheels/tensorflow-1.10.1-cp35-cp35m-linux_aarch64.whl
fi





