import numpy as np
import os
import sys
import pickle
import time
import glob
from prefetch_generator import BackgroundGenerator
import random
from model.losses import bce_dice_loss, dice_coeff, dice_loss

import tensorflow as tf
import keras.backend as K

from keras.preprocessing.image import ImageDataGenerator

import numpy as np

from PIL import Image, ImageDraw
import cv2

from keras.models import Model, Sequential, load_model

from keras.layers import Lambda, Input

from keras.callbacks import Callback

import threading
from model.mobile_u_net import get_unet_MDCB_max128filters_with_deconv_and_add_layers_with_batch_normalization

from skimage.measure import regionprops, label

from skimage.transform import resize as resize_skimage

from skimage.morphology import remove_small_objects
from queue import Queue
from copy import deepcopy
from datetime import datetime
import matplotlib.pyplot as plt
from scipy import misc
print("imports done")

#tf.keras.backend.set_learning_phase(0)
#K.set_image_data_format("channels_first")

DEBUG = False

num_cores = 1
num_CPU = 1
num_GPU = 1

REGIONPROPS_SCALE_FACTOR = 2

THRESHOLD = 0.5
MIN_MASK_AREA = 80 /(REGIONPROPS_SCALE_FACTOR*REGIONPROPS_SCALE_FACTOR)
MIN_HEAD_AREA = 12 /(REGIONPROPS_SCALE_FACTOR*REGIONPROPS_SCALE_FACTOR)
MIN_FEET_AREA = 12 /(REGIONPROPS_SCALE_FACTOR*REGIONPROPS_SCALE_FACTOR)
HEAD_FEET_ANGLE_DEG = 0
HEAD_FEET_ANGLE_DEG_FLIPPED_ZERO = 0

WEIRD_HEAD_FEET_ANGLE_THRESHOLD = 10

CONFIDENCE_SCORE_HEAD_AND_FEET_SINGLE = 1
CONFIDENCE_SCORE_HEAD_AND_FEET_IN_GROUP = 0.9

CONFIDENCE_SCORE_NO_HEAD_FEET = 0.8



# ERTI test path
TEST_PATH = '/images'

# ERTI txt boxes path
OUTPUT_PATH_TXT = '/results'

MODEL_WEIGHTS_PATH = 'model_weights.hdf5'

# temp out path
OUT_PATH = 'results_box'

image_list = os.listdir(TEST_PATH)

if not os.path.exists(OUT_PATH):
    os.makedirs(OUT_PATH)


globalResults = Queue()

config_session = tf.ConfigProto(intra_op_parallelism_threads=num_cores,\
        inter_op_parallelism_threads=num_cores, allow_soft_placement=True,\
        device_count = {'CPU' : num_CPU, 'GPU' : num_GPU}, gpu_options = {'per_process_gpu_memory_fraction' : 0.4})
#       
#config_session = tf.ConfigProto(intra_op_parallelism_threads=num_cores,\
#        inter_op_parallelism_threads=num_cores, allow_soft_placement=True,\
#        device_count = {'CPU' : num_CPU, 'GPU' : num_GPU}, gpu_options = {'allow_growth': True})
session = tf.Session(config=config_session)
K.set_session(session)


input_size_height = 544
input_size_width = 960
batch_size = 1
SCALE_FACTOR = 2

desired_input_size_height = 448
desired_input_size_width = 832
# diffs = input_size_height - desired_input_size_height, input_size_width - desired_input_size_width


SCALE_FACTOR_WIDTH = 1920/input_size_width
SCALE_FACTOR_HEIGHT = 1080/input_size_height

model = get_unet_MDCB_max128filters_with_deconv_and_add_layers_with_batch_normalization(input_shape=(desired_input_size_height, desired_input_size_width, 3), lr=0.001, num_output_classes=4)
model.summary()

#load weights
model.load_weights(filepath=MODEL_WEIGHTS_PATH)


x_batch = []

def npGetInfo(item):
    if not item is None:
        return item.shape, np.min(item), np.max(item), np.mean(item), np.std(item), item.dtype
    else:
        return None

def get_weights(model, layer_name):
    W = model.get_layer(name=layer_name).get_weights()
    if len(W) > 0:
        W = np.array(W[0])
    else:
        W = None
    return W

def angle_between(p1, p2):
    head_cnt = p1
    feet_cnt = p2
    angle = np.absolute((np.arctan2(head_cnt[0] - feet_cnt[0], head_cnt[1] - feet_cnt[1]) + np.pi/2) * 180 / np.pi)
    return angle
    
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]
    
def find_nearest_idx(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def doBboxesInterect(bbox1, bbox2):
    a, b = bbox1, bbox2
    intersection_area = max(0, min(a[3], b[3]) - max(a[1], b[1])) * max(0, min(a[2], b[2]) - max(a[0], b[0]))
    if intersection_area > 0:
        return True
    else:
        return False

def evaluatePrediction(currentPredictionImage):

    currentPredictionImage = resize_skimage(currentPredictionImage, (input_size_height/REGIONPROPS_SCALE_FACTOR, input_size_width/REGIONPROPS_SCALE_FACTOR), anti_aliasing=False, preserve_range=True)
    
    # TODO: feet for confidence
    start_pred_bbox_time = time.time()
    output_boxes = []

    # segmentation maps
    current_masks = currentPredictionImage[:, :, 0]
    current_heads = currentPredictionImage[:, :, 1]
    # current_body = currentPredictionImage[:, :, 2]
    current_feet = currentPredictionImage[:, :, 3]
    
    start_label_time = time.time()
    
    # label regions
    label_masks = label(current_masks)
    label_heads = label(current_heads)
    # label_body = label(current_body)
    label_feet = label(current_feet)
    
    start_regionprops_time = time.time()

    # extract regionprops
    rp_masks = regionprops(label_masks)
    rp_heads = regionprops(label_heads)
    # rp_body = regionprops(label_body)
    rp_feet = regionprops(label_feet)
    
    start_region_filtering = time.time()

    # filter small regions
    rp_masks = [rp for rp in rp_masks if rp.area > MIN_MASK_AREA]
    rp_heads = [rp for rp in rp_heads if rp.area > MIN_HEAD_AREA]
    # rp_body = [rp for rp in rp_body if rp.area > MIN_BODY_AREA]
    rp_feet = [rp for rp in rp_feet if rp.area > MIN_FEET_AREA]

    start_centroid_and_bboxes = time.time()

    # region data
    heads_centroids = [x.centroid for x in rp_heads]
    heads_bboxes = [x.bbox for x in rp_heads]
    mask_bboxes = [x.bbox for x in rp_masks]
    feet_centroids = [x.centroid for x in rp_feet]
    feet_bboxes = [x.bbox for x in rp_feet]

    seen_mask_bboxes = np.zeros(len(mask_bboxes))

    
    object_mask_changed = False
    first_pass = True
    
    start_for = time.time()

    for idx_rp_head in range(len(heads_bboxes)):
        # check head/mask intersection
            for idx_rp_mask in range(len(mask_bboxes)):
                if doBboxesInterect(heads_bboxes[idx_rp_head], mask_bboxes[idx_rp_mask]):
                    #print('intersection here')
                    current_box = heads_bboxes[idx_rp_head]
                    #output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], 0.3))
                    # find closest intersecting feet
                    rp_intersecting_feet = []
                    rp_intersecting_feet_angle = []
                    for idx_rp_foot in range(len(feet_bboxes)): 
                        if doBboxesInterect(feet_bboxes[idx_rp_foot], mask_bboxes[idx_rp_mask]):
                            rp_intersecting_feet.append(feet_bboxes[idx_rp_foot])
                            rp_intersecting_feet_angle.append(angle_between(heads_centroids[idx_rp_head], feet_centroids[idx_rp_foot]))
                    #print('lif', len(rp_intersecting_feet))
                    if len(rp_intersecting_feet) ==1: # single feet? 
                        #print('single feet')
                        current_box = mask_bboxes[idx_rp_mask]
                        #current_box = rp_foot.bbox
                        #output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], 0.4))
                        output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], CONFIDENCE_SCORE_HEAD_AND_FEET_SINGLE))
                        # remove from original image
                        #current_masks[current_box[0]:current_box[2],current_box[1]:current_box[3]] = 0
                        #object_mask_changed = True
                        seen_mask_bboxes[idx_rp_mask] = 1
                        #break
                    elif len(rp_intersecting_feet) > 1:
                        #group
                        closest_foot_idx = find_nearest_idx(rp_intersecting_feet_angle, HEAD_FEET_ANGLE_DEG)
                        #print('intersecting angles', rp_intersecting_feet_angle)
                        #print('intersecting angle', rp_intersecting_feet_angle[closest_foot_idx])
                        if abs(rp_intersecting_feet_angle[closest_foot_idx] - HEAD_FEET_ANGLE_DEG_FLIPPED_ZERO) > WEIRD_HEAD_FEET_ANGLE_THRESHOLD:
                            #print('intersecting angle', rp_intersecting_feet_angle[closest_foot_idx])
                            continue # better luck next time
                        current_foot_box = rp_intersecting_feet[closest_foot_idx]
                        # make box from head and feet limits
                        #print('here')
                        head_bbox = heads_bboxes[idx_rp_head]
                        foot_bbox = current_foot_box
                        minr_temp = min(head_bbox[0], foot_bbox[0])
                        minc_temp = min(head_bbox[1], foot_bbox[1])
                        maxr_temp = max(head_bbox[2], foot_bbox[2])
                        maxc_temp = max(head_bbox[3], foot_bbox[3])

                        BOX_WIDTH_TO_HEIGHT = 0.04
                        height = maxr_temp - minr_temp
                        width_extra = int(height * BOX_WIDTH_TO_HEIGHT)
                        minc_temp = minc_temp - width_extra
                        maxc_temp = maxc_temp + width_extra
                        if minc_temp < 0:
                            minc_temp = 0
                        if maxc_temp > input_size_width:
                            maxc_temp = input_size_width - 1

                        output_boxes.append((minr_temp, minc_temp, maxr_temp, maxc_temp, CONFIDENCE_SCORE_HEAD_AND_FEET_IN_GROUP))
                        #print((minr_temp, minc_temp, maxr_temp, maxc_temp, CONFIDENCE_SCORE_HEAD_AND_FEET_IN_GROUP))
                        # remove from original image
                        #print(np.max(current_masks[minr_temp:maxr_temp,minc_temp:maxc_temp]))
                        current_masks[minr_temp:maxr_temp,minc_temp:maxc_temp] = 0
                        #current_masks[minc_temp:maxc_temp,minr_temp:maxr_temp] = 0
                        #object_mask_changed = True
                        #break
                        seen_mask_bboxes[idx_rp_mask] = 1


    
    #assign a box for everything left (no head/feet)
    for idx_rp_mask in range(len(mask_bboxes)):
        current_box = mask_bboxes[idx_rp_mask]
        # add confidence...
        if seen_mask_bboxes[idx_rp_mask] == 0:
            output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], CONFIDENCE_SCORE_NO_HEAD_FEET))
            
    end_for = time.time()
    
    end_pred_bbox_time = time.time()

    elapsed_pred_bbox_time = end_pred_bbox_time - start_pred_bbox_time
    
    #print('time:label:{};rp:{};filtering:{};centroid+bbox:{};for:{}'.format(start_regionprops_time-start_label_time, \
    #                                    start_region_filtering-start_regionprops_time, \
    #                                    start_centroid_and_bboxes-start_region_filtering, \
    #                                    start_for-start_centroid_and_bboxes, \
    #                                    end_for-start_for \
    #                                    ))
    #print('elapsed label time:', elapsed_pred_bbox_time)
    
    return output_boxes

def evaluatePredictionNew(currentPredictionImage):
    
    #currentPredictionImage = resize_skimage(currentPredictionImage, (input_size_height/REGIONPROPS_SCALE_FACTOR, input_size_width/REGIONPROPS_SCALE_FACTOR), anti_aliasing=False, preserve_range=True)
    # TODO: feet for confidence
    start_pred_bbox_time = time.time()
    output_boxes = []

    # segmentation maps
    current_masks = currentPredictionImage[:, :, 0]
    current_heads = currentPredictionImage[:, :, 1]
    # current_body = currentPredictionImage[:, :, 2]
    current_feet = currentPredictionImage[:, :, 3]
    
    #filtering
    #current_heads = np.asarray(Image.fromarray(current_heads, mode='L').filter(ImageFilter.GaussianBlur(2))) > SOFT_THRESH
    #current_feet = np.asarray(Image.fromarray(current_feet, mode='L').filter(ImageFilter.GaussianBlur(2))) > SOFT_THRESH
    
    start_label_time = time.time()
    
    # label regions
    label_masks = label(current_masks)
    label_heads = label(current_heads)
    # label_body = label(current_body)
    label_feet = label(current_feet)
    
    start_regionprops_time = time.time()

    # extract regionprops
    rp_masks = regionprops(label_masks)
    rp_heads = regionprops(label_heads)
    # rp_body = regionprops(label_body)
    rp_feet = regionprops(label_feet)
    
    start_region_filtering = time.time()

    # filter small regions

    rp_masks = [rp for rp in rp_masks if rp.area > MIN_MASK_AREA]
    rp_heads = [rp for rp in rp_heads if rp.area > MIN_HEAD_AREA]
    # rp_body = [rp for rp in rp_body if rp.area > MIN_BODY_AREA]
    rp_feet = [rp for rp in rp_feet if rp.area > MIN_FEET_AREA]
    

    start_centroid_and_bboxes = time.time()

    # region data
    heads_centroids = [x.centroid for x in rp_heads]
    heads_bboxes = [x.bbox for x in rp_heads]
    mask_bboxes = [x.bbox for x in rp_masks]
    feet_centroids = [x.centroid for x in rp_feet]
    feet_bboxes = [x.bbox for x in rp_feet]

    seen_mask_bboxes = np.zeros(len(mask_bboxes))
    

    for idx_rp_head in range(len(heads_bboxes)):
        # check head/mask intersection
            for idx_rp_mask in range(len(mask_bboxes)):
                if doBboxesInterect(heads_bboxes[idx_rp_head], mask_bboxes[idx_rp_mask]):
                    #print('intersection here')
                    current_box = heads_bboxes[idx_rp_head]
                    #output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], 0.3))
                    # find closest intersecting feet
                    rp_intersecting_feet_bbox = []
                    rp_intersecting_feet_centroid = []
                    rp_intersecting_feet_angle = []
                    for idx_rp_foot in range(len(feet_bboxes)):
                        if doBboxesInterect(feet_bboxes[idx_rp_foot], mask_bboxes[idx_rp_mask]):
                            rp_intersecting_feet_bbox.append(feet_bboxes[idx_rp_foot])
                            rp_intersecting_feet_centroid.append(feet_centroids[idx_rp_foot])
                            rp_intersecting_feet_angle.append(angle_between(heads_centroids[idx_rp_head], feet_centroids[idx_rp_foot]))
                    #print('lif', len(rp_intersecting_feet))
                    if len(rp_intersecting_feet_bbox) ==1: # single feet? 
                        #print('single feet')
                        current_box = mask_bboxes[idx_rp_mask]
                        #current_box = rp_foot.bbox
                        #output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], 0.4))
                        output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], CONFIDENCE_SCORE_HEAD_AND_FEET_SINGLE, idx_rp_mask))
                        # remove from original image
                        #current_masks[current_box[0]:current_box[2],current_box[1]:current_box[3]] = 0
                        #object_mask_changed = True
                        seen_mask_bboxes[idx_rp_mask] = 1
                        #break
                    elif len(rp_intersecting_feet_bbox) > 1:
                        #group
                        closest_foot_idx = find_nearest_idx(rp_intersecting_feet_angle, HEAD_FEET_ANGLE_DEG)
                        #print('intersecting angles', rp_intersecting_feet_angle)
                        #print('intersecting angle', rp_intersecting_feet_angle[closest_foot_idx])
                        if abs(rp_intersecting_feet_angle[closest_foot_idx] - HEAD_FEET_ANGLE_DEG_FLIPPED_ZERO) > WEIRD_HEAD_FEET_ANGLE_THRESHOLD:
                            #print('intersecting angle', rp_intersecting_feet_angle[closest_foot_idx])
                            continue # better luck next time
                        current_foot_box = rp_intersecting_feet_bbox[closest_foot_idx]
                        # make box from head and feet limits
                        #print('here')
                        head_bbox = heads_bboxes[idx_rp_head]
                        foot_bbox = current_foot_box
                        minr_temp = min(head_bbox[0], foot_bbox[0])
                        minc_temp = min(head_bbox[1], foot_bbox[1])
                        maxr_temp = max(head_bbox[2], foot_bbox[2])
                        maxc_temp = max(head_bbox[3], foot_bbox[3])
                        
                        BOX_WIDTH_TO_HEIGHT = 0.04
                        height = maxr_temp - minr_temp
                        width_extra = int(height * BOX_WIDTH_TO_HEIGHT)
                        minc_temp = minc_temp - width_extra
                        maxc_temp = maxc_temp + width_extra
                        if minc_temp < 0:
                            minc_temp = 0
                        if maxc_temp > input_size_width:
                            maxc_temp = input_size_width - 1
                        
                        output_boxes.append((minr_temp, minc_temp, maxr_temp, maxc_temp, CONFIDENCE_SCORE_HEAD_AND_FEET_IN_GROUP))
                        seen_mask_bboxes[idx_rp_mask] = 1


    
    #assign a box for everything left (no head/feet)
    for idx_rp_mask in range(len(mask_bboxes)):
        current_box = mask_bboxes[idx_rp_mask]
        # add confidence...
        if seen_mask_bboxes[idx_rp_mask] == 0:
            output_boxes.append((current_box[0], current_box[1], current_box[2], current_box[3], CONFIDENCE_SCORE_NO_HEAD_FEET))
    output_boxes_final = []
    # split wide single head and feet boxes
    
    single_widths = []
    num_samples = 0
    
    end_pred_bbox_time = time.time()

    elapsed_pred_bbox_time = end_pred_bbox_time - start_pred_bbox_time
    
    #print('elapsed label time:', elapsed_pred_bbox_time)
    
    return output_boxes
    
def drawBoxesPIL(currentRGBImage, boxes):
    draw = ImageDraw.Draw(currentRGBImage)
    for box in boxes:
        #print(box)
        #if box[4] ==0.3:
        #    cv2.rectangle(currentRGBImage, (int(box[1]), int(box[0])), (int(box[3]), int(box[2])), (0,0,255,255), 2, 8)
        #elif box[4] == 0.4:
        #    cv2.rectangle(currentRGBImage, (int(box[1]), int(box[0])), (int(box[3]), int(box[2])), (0,255,0,255), 2, 8)
        #else:
        #    cv2.rectangle(currentRGBImage, (int(box[1]), int(box[0])), (int(box[3]), int(box[2])), (255,255,255,255), 2, 8)
        draw.rectangle(((int(SCALE_FACTOR_WIDTH*box[3]), int(SCALE_FACTOR_HEIGHT*box[2])),(int(SCALE_FACTOR_WIDTH*box[1]), int(SCALE_FACTOR_HEIGHT*box[0]))), outline="white")
        
    return currentRGBImage

def readImageSafe(path):
    with open(path, "rb") as f:
        image = Image.open(f)
        image.load()
    return image

class ConsumerThread(threading.Thread):
    def __init__(self, allImagesList): 
        super(ConsumerThread,self).__init__() 
        self.allImagesList = allImagesList
        self.totalNumItems = len(allImagesList)
        print("Starting Consumer thread with %d items." % self.totalNumItems)

    def run(self):
        counter = 0
        while True: 
            try:
                item = globalResults.get()
            except queue.Empty:
                time.sleep(0.1)
                continue

            for current_result_map in item: 
                assert counter < self.totalNumItems 
                current_name = os.path.basename(self.allImagesList[counter])
                output_txt_name = current_name.replace('.jpg', '.txt')
                #print("current_result_shape: %s. current output_txt_name: %s. global_ix: %d" % (current_result_map.shape, output_txt_name, counter)) 
                out_boxes_file = open(os.path.join(OUTPUT_PATH_TXT, output_txt_name), 'w')
                res = current_result_map > THRESHOLD
                current_boxes = evaluatePrediction(res)
                if DEBUG:
                    currentRGBImage = readImageSafe(self.allImagesList[counter]).resize((input_size_width, input_size_height))
                    currentRGBImage = drawBoxesPIL(currentRGBImage, current_boxes)
                    currentRGBImage.save('{0}/{1}'.format(OUTPUT_PATH_TXT, current_name))

                for box in current_boxes:
                    #print('written box')
                    out_boxes_file.write('{},{},{},{},{}\n'.format(int(SCALE_FACTOR_WIDTH*box[1]), int(SCALE_FACTOR_HEIGHT*box[0]), int(SCALE_FACTOR_WIDTH*(box[3]-box[1])), int(SCALE_FACTOR_HEIGHT*(box[2]-box[0])),box[4]))
    
                out_boxes_file.close()
                counter += 1        

            if counter == self.totalNumItems:
                print("Finishing consumer thread!")
                return

class JPGReader:
    def __init__(self, datasetPath, inShape, cropShape):
        self.images = sorted(glob.glob("%s/*.jpg" % (datasetPath)))
        self.inShape = np.array(inShape)
        self.cropShape = np.array(cropShape)
        diffs = self.inShape - self.cropShape
        assert np.sum(diffs == 0) == 0
        self.cropBox = (diffs[0] // 2, diffs[1] // 2, \
            inShape[0] - (diffs[0] // 2 + (diffs[0] % 2 != 0)), \
            inShape[1] - (diffs[1] // 2 + (diffs[1] % 2 != 0)))

    # def readImages(self, imagesPath):
    #     imgs = [np.array(readImageSafe(i).resize(self.inShape).crop(self.cropBox)) for i in imagesPath]
    #     imgs = np.float32(np.array(imgs)) / 255
    #     return imgs

    def readImages(self, imagesPath):
        imgs = [cv2.resize(cv2.imread(i), (self.inShape[0], self.inShape[1])) for i in imagesPath]
        imgs = np.float32(np.array(imgs)) / 255
        # BGR to RGB
        # imgs = imgs[..., ::-1]
        imgs = imgs[:, self.cropBox[1] : self.cropBox[3], self.cropBox[0] : self.cropBox[2]]
        return imgs

    def getNumIterations(self, type, miniBatchSize, accountTransforms=True):
        N = len(self.images) // miniBatchSize + (len(self.images) % miniBatchSize != 0)
        return N

    def iterate_once(self, type, miniBatchSize):
        numIterations = self.getNumIterations(type, miniBatchSize, accountTransforms=False)

        for i in range(numIterations):
            startIndex = i * miniBatchSize
            endIndex = min((i + 1) * miniBatchSize, len(self.images))
            assert startIndex < endIndex, "startIndex < endIndex. Got values: %d %d" % (startIndex, endIndex)
            numData = endIndex - startIndex
                
            yield self.readImages(self.images[startIndex : endIndex])
            #yield np.random.randn(numData, self.inShape[0], self.inShape[1], 3).astype(np.float32)
  
    def iterate(self, type, miniBatchSize, maxPrefetch=0):
        assert maxPrefetch >= 0
        while True:
            iterateGenerator = self.iterate_once(type, miniBatchSize)
            if maxPrefetch > 0:
                iterateGenerator = BackgroundGenerator(iterateGenerator, max_prefetch=maxPrefetch)
            for items in iterateGenerator:
                yield items
                del items

#test_datagen = ImageDataGenerator(rescale=1./255)
       
start_time = time.time()

MB = 4
reader = JPGReader(TEST_PATH, (input_size_width, input_size_height), \
    (desired_input_size_width, desired_input_size_height))
cropBox = reader.cropBox
generator = reader.iterate(None, miniBatchSize=MB, maxPrefetch=2)
numSteps = reader.getNumIterations(None, miniBatchSize=MB)
ConsumerThread(deepcopy(reader.images)).start()

nowStart = datetime.now()
i = 0
numItems = 0
newPred = np.zeros((MB, input_size_height, input_size_width, 4)).astype(np.float32)
cntJumped = 0
for j in range(numSteps):
    if j > 0:
        fps = MB / took.total_seconds()
        totalFps = numItems / (datetime.now() - nowStart).total_seconds()
        # avgFps = ((avgFps * i) + fps) / (i + 1)
        print("[%d/%d] Took %s. FPS: %2.2f. Avg FPS: %2.2f. Shape: %s" % (i, numSteps, took, fps, totalFps, queuedShape))
    #print(next(generator).shape)
    now = datetime.now()

    item = next(generator)
    thisMB = item.shape[0]
    if i > 500 and totalFps < 5.1 and i % 5 == 0:
        pred = np.expand_dims(pred[-1], axis=0)
        cntJumped += thisMB
        print("Copying prev result")
    else:
        pred = model.predict_on_batch(item)
    newPred *= 0
    newPred[0 : thisMB, cropBox[1] : cropBox[3], cropBox[0] : cropBox[2]] = pred
    queuedPred = np.copy(newPred[0 : thisMB])
    queuedShape = queuedPred.shape
    i += 1
    numItems += queuedShape[0]
    
    took = datetime.now() - now

    globalResults.put(queuedPred)

end_time = time.time()

print('elapsed: {} s'.format(end_time-start_time))
