## The wheels are compiled with:



#### CUDA 8.0 + CUDNN 6.0.21:
 
tensorflow-1.3.0-cp35-cp35m-linux_aarch64.whl

#### CUDA 8.0 + CUDNN 7.0.1:
 
tensorflow-1.10.0-cp35-cp35m-linux_aarch64.whl

#### CUDA 9.0 + CUDNN 7.1.2:

tensorflow-1.10.1-cp35-cp35m-linux_aarch64.whl

Disclaimer:
- the tf 1.3.0 wheel was retrieved from https://github.com/mohitkumarahuja/Install-TensorFlow-and-Keras-on-Jetson-TX2-with-JetPack-3.1/blob/master/Wheel%20File%20for%20TX2/tensorflow-1.3.0-cp35-cp35m-linux_aarch64.whl

- the 1.10.1 wheel was provided by nvidia, but the link got deleted since I downloaded it

- the 1.10.0 wheel was compiled on the TX2 using cudnn from https://developer.nvidia.com/compute/machine-learning/tensorrt/secure/3.0/rc1/nv-tensorrt-repo-ubuntu1604-rc-cuda8.0-trt3.0-20170922_3.0.0-1_arm64-deb :

   `sudo dpkg -i nv-tensorrt-repo-ubuntu1604-rc-cuda8.0-trt3.0-20170922_3.0.0-1_arm64.deb`
   
   `sudo apt update`
   
   `sudo apt install libcudnn7-dev`
