# ERTI challenge submission


Team name: **stormtroopers-erti**

Expected environment: CUDA 8.0 + CUDNN 6.0.21  **or**  CUDA 8.0 + CUDNN 7.0.1  **or** CUDA 9.0 + CUDNN 7.1.2

# 0. Install dependencies

Execute `install_deps.sh`

For CUDA 8 + CUDNN 7.0.1, see the readme inside the tf_wheels folder (you need to download and install a deb from nvidia's website).

# 1. Running the code:

Execute `run.sh`.

It needs a readable `/images` folder with jpeg images and writable `/results` folder, where it will output `x,y,width,height,confidence_score` detections.

It needs opencv-python3 installed.
 
# 2. Samples

A small sample set on the footage provided by the ERTI organizers can be found in the `/samples` folder.

# 3. Issues

We encountered a memory issue caused by **CUDA 9**. More details at https://devtalk.nvidia.com/default/topic/1037898/tensorflow-batch_to_space_nd-not-working-for-large-channel-sizes-on-tx2/ .

The code produces good results running on CPU using `CUDA_VISIBLE_DEVICES=""` before the python command inside the run.sh. 
